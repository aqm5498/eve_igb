var eveHeader = require('eve-header');
var helmet = require('helmet');
var express = require('express');
var app = express();

app.use(eveHeader);
app.use(helmet());
app.use(express.static(__dirname+ '/public'));

app.set('view engine', 'jade');
app.get('/', function(req,res,next){
    var pilot = req.eve.char.name;
    var ship = req.eve.ship.name;
    res.render('index', {title: req.eve.char.name,  pilot: pilot });
    res.end();
});

app.listen(8080);
